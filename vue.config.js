module.exports = {
  pwa: {
    themeColor: '#000000',
    name: 'The Caravan',
    msTileColor: '#000000'
  },
  devServer: {
    disableHostCheck: true
  }
}