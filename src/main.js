import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import './assets/styles/bulma.sass'
import VueAffix from 'vue-affix'
import VueLazyload from 'vue-lazyload'

Vue.use(VueAffix)
Vue.config.productionTip = false

Vue.use(VueLazyload, {
  preLoad: 1.3,
  attempt: 1,
  // set observer to true
  observer: true,

  // optional
  observerOptions: {
    rootMargin: '100px 100px 100px 100px',
    threshold: 0.1
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
